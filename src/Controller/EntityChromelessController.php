<?php

namespace Drupal\entity_chromeless\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EntityChromelessController extends ControllerBase {

  /**
   * Displays the markup.
   *
   * @return array
   */
   public function content($entity_type, $entity_id, $display_mode) {
     $entity_storage = \Drupal::entityTypeManager()->getStorage($entity_type);
     if (!$entity_storage) {
       throw new NotFoundHttpException();
     }
     $entity = $entity_storage->load($entity_id);
     if (!$entity) {
       throw new NotFoundHttpException();
     }
     $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
     $pre_render = $view_builder->view($entity, $display_mode);
     return $pre_render;
   }

}

