# Entity Chromeless

This module provides a route to display entities on their own (similar to the default view route).

An example page template is provided to show the main content without any other regions.

This is useful for embedding media in colorboxes, for example.

# Installation

1. Download into module directory or use composer
2. Copy page--chromeless.html.twig.example to your theme's template folder and modify if necessary
3. Clear all caches

# License

This module is licensed under GPLv2+.
